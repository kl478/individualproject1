+++
title = "Personal Website"


# The homepage contents
[extra]
lead = 'This is a website about <b>Kejia Liu</b> (AKA, Rebecca Liu).'
url = "/docs/getting-started/introduction/"
url_button = "My portfolio"
repo_version = "GitHub v0.1.0"
repo_license = "Open-source MIT License."
repo_url = "https://github.com/aaranxu/adidoks"

# Menu items
[[extra.menu.main]]
name = "Portfolio"
section = "docs"
url = "/docs/getting-started/introduction/"
weight = 10

[[extra.menu.main]]
name = "Blog"
section = "blog"
url = "/blog/"
weight = 20

[[extra.list]]
title = "Pioneering Woman"
content = "As a female coder, I'm inspired by Ada Lovelace, the first programmer, known for her work on Babbage's early computer."

[[extra.list]]
title = "Hello, World!"
content = "Embracing the programmer's rite, my first code mirrored the 'Hello, World!' tradition from Kernighan and Ritchie's 1978 book."

[[extra.list]]
title = "Bug's Beginnings"
content = "In programming, encountering bugs, a term born from a moth found in a Harvard computer in 1947, is a norm for me."

[[extra.list]]
title = "Python's Genesis"
content = "Python, my first programming language, owes its name not to the reptile but to the humor of Monty Python."

[[extra.list]]
title = "Java's Roots"
content = "My Java journey began in undergrad. Java was named aptly after the Indonesian island, a nod to coffee aficionados."

[[extra.list]]
title = "Swift's Secret"
content = "In ECE 564 Mobile App Development, I embraced Swift—a language Apple crafted in secrecy for years—to build iOS apps."

+++
