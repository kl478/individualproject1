+++
title = "Programming Projects"
description = "Contribute to AdiDoks, improve documentation, or submit to showcase."
date = 2021-05-01T18:10:00+00:00
updated = 2021-05-01T18:10:00+00:00
draft = false
weight = 410
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Some of my personal projects."
toc = true
top = false
+++

## Formula 1 Database Application

👉 [Github Repo](https://github.com/Reby0217/Formula-1-F1-Database-Application)

- Devised a Formula 1 application connected to the UBC CS department's Oracle database, including information on races, drivers, constructors, etc.

- Implemented the following functionalities: Insertion, deletion, update, selection, join, projection, division, aggregation with group by, aggregation with having, nested aggregation with group by, using SQL

- Constructed GUIs with PHP, HTML, and CSS, allowing users to interact with the database, such as clicking buttons to manage the database and execute the queries

- Created cascade tables to help manage the hierarchy


## Personal Website

👉 [GitLab Repo](https://gitlab.com/kl478/miniproject1)

- Website built with Zola

- GitLab workflow to build and deploy site on push