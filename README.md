# IDS721 Individual Project

This project is built using Zola, a fast static site generator, and is designed to present my professional portfolio and projects.

[Demo video](https://gitlab.com/kl478/individualproject1/-/blob/main/DemoVideo.mp4)

## Deployment

The site is automatically deployed via GitLab CI/CD upon pushing to the repository.
- GitLab Pages URL: [https://individualproject1-kl478-dde4232e7b7724ffb41a945357201a2b89ab62.gitlab.io/](https://individualproject1-kl478-dde4232e7b7724ffb41a945357201a2b89ab62.gitlab.io/)

This site is also hosted on Netlify.
- Netlify URL: [https://reby0217.netlify.app](https://reby0217.netlify.app)



## Features

- **Home**: Dive into a brief intro about my inspiration and start in programming.
- **Personal Info**: Learn about my educational background and skill set.
- **Albums**: Browse through my cherished memories and experiences.
- **Programming Projects**: Explore the projects I've worked on, detailing my technical capabilities.
- **My Research**: Delve into my research work and contributions to deep learning and data science.
- **FAQ**: Find answers to common questions and learn how to get in touch.

## Project Structure

- **Content**: Markdown files for web content.
- **Static**: Static assets like images and compiled CSS.
- **Templates**: HTML layouts and components.
- **Themes**: AdiDoks theme for aesthetic and functionality.


## Key Steps
1. Install [Zola](https://www.getzola.org/documentation/getting-started/installation/).


2. Set up your Zola site:

```bash
zola init mysite
```

3. Install the AdiDoks theme:

You can add the theme to your `themes` directory:

```bash
cd mysite/themes
git clone https://github.com/aaranxu/adidoks.git
```

Or as a submodule:

```bash
cd mysite
git init  # Skip if you have already initialized git
git submodule add https://github.com/aaranxu/adidoks.git themes/adidoks
```

4. Configure your site:

Enable the theme in your `config.toml`:

```toml
theme = "adidoks"
```

Or use the example configuration provided by the theme:

```bash
cp themes/adidoks/config.toml.example config.toml
```

5. Add content:

Copy the example content to your site:

```bash
cp -r themes/adidoks/content .
```

6. Customize or add new content to the `content` directory as needed.

7. Running Locally

To see your site in action, run Zola's built-in development server:

```bash
zola serve
```

Visit `http://127.0.0.1:1111/` in your browser to see the site.

8. The `.gitlab-ci.yml` file defines the CI/CD process, which automatically builds and updates the site on each push to the GitLab repository.

9. Host on `Netlify`:
    - Create a Netlify account if you don't have one.
    - Connect your GitLab repository to Netlify. Netlify will listen for changes to your repository.
    - Adding a `netlify.toml` file to your repository to customize the build process on Netlify.
    - Then trigger deploy.



